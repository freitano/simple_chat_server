const chalk = require('chalk');
const { clients, getRandomColor, getRandomName } = require('./connectionManager');
const EOL = '\r\n';

const multicast = (sender=null) => (message) => {
  let [red, green, blue] = sender ? sender.color : [255, 0, 0];
  let senderName = sender ? sender.name : 'ADMIN';
  message = trim(message);
  console.log('message from ', senderName);
  for (key in clients) {
    if (sender != clients[key]) {
      clients[key].write(chalk.rgb(red, green, blue)(`${senderName}: ${message}${EOL}`));
    }
  }
};
const broadcast = multicast();

function send(message, recipient) {
  recipient.write(message+EOL);
};

function trim(message) {
  return message.replace(/\r\n$/, '');
}

module.exports.handle = function(client) {
  console.log('new client connected!');
  send('you entered the chat', client);
  client.setEncoding('utf-8');
  client.name = getRandomName(6);
  client.color = getRandomColor();
  send(`your id is: ${client.name}`, client);
  broadcast(`${client.name} joined the chat`);
  client.on('data', multicast(client));

  client.on('close', function() {
    console.log(`client ${client.name} is leaving`);
    let index = clients.indexOf(client);
    if(index > -1)
      clients.splice(index, 1);
    broadcast(`${client.name} left the chat`);
    client.destroy();
    console.log('connected clients = ', clients.length);
  });
  clients.push(client);
};

module.exports.multicast = multicast;
module.exports.broadcast = broadcast;
