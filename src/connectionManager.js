const clientHandler = require('./clientHandler');

const clients = [];

function randomColor() {
  const r = Math.floor(Math.random() * 256);
  const g = Math.floor(Math.random() * 256);
  const b = Math.floor(Math.random() * 256);
  return [r, g, b];
}

function randomName(size) {
  return Math.random().toString(36).substring(size);
};

module.exports.clients = clients;
module.exports.getRandomColor = randomColor;
module.exports.getRandomName = randomName;
