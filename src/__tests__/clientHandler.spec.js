const { handle, multicast, broadcast } = require('../clientHandler');

const createMockedClient = () => ({
  write: jest.fn(),
  on: jest.fn(),
  setEncoding: jest.fn(),
  destroy: jest.fn()
});

const mockedClient = createMockedClient();

beforeAll(() => {
  handle(mockedClient);
});

afterAll(() => {
  jest.resetAllMocks();
});

describe('clientHandler', () => {
  it('should receive welcome messages when connected', () => {
    expect(mockedClient.write).toHaveBeenCalledTimes(2);
    expect(mockedClient.write).toHaveBeenNthCalledWith(1, 'you entered the chat\r\n');
    expect(mockedClient.write).toHaveBeenNthCalledWith(2, expect.stringMatching(/^your id is: [a-z0-9]+/));
  });
  it('should receive a call to "data" and "close"', () => {
    expect(mockedClient.on).toHaveBeenCalledTimes(2);
    expect(mockedClient.on).toHaveBeenNthCalledWith(1, 'data', expect.any(Function));
    expect(mockedClient.on).toHaveBeenNthCalledWith(2, 'close', expect.any(Function));
  });
});
