const { clients, getRandomColor, getRandomName } = require('../connectionManager');

describe('connectionManager', () => {
  it('clients should be an empty array', () => {
    expect(clients).toEqual([]);
  });
  it('getRandomColor should return an array of 3 number in the range [0,255]', () => {
    let [red, green, blue] = getRandomColor();
    expect(red).toBeGreaterThanOrEqual(0);
    expect(red).toBeLessThanOrEqual(255);
    expect(green).toBeGreaterThanOrEqual(0)
    expect(green).toBeLessThanOrEqual(255);
    expect(blue).toBeGreaterThanOrEqual(0)
    expect(blue).toBeLessThanOrEqual(255);
  });
  it('getRandomName should return an alphanumeric string', () => {
    let name = getRandomName(5);
    expect(name).toMatch(/[a-z0-9]{5,5}/);
  });
});
