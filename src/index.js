require('dotenv').config();
const server = require('./server');

const SERVER_PORT = process.env.SERVER_PORT || 10000;

server.listen(SERVER_PORT, () => {
  console.log(`server started on port ${SERVER_PORT}`);
  console.log('waiting for connections');
});
