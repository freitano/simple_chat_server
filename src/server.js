const net = require('net');
const clients = [];

const server = net.createServer();
const { handle } = require('./clientHandler');

server.on('connection', handle);
server.on('close', () => {
  console.log('Server disconnected');
});
server.on('error', error => {
  console.log(`Error : ${error}`);
});

module.exports = server;
