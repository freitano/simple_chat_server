# Simple Telnet Chat
This is a simple chat server, with a single chat room a no user credentials required. It broadcasts every message submitted from a client to all other connected clients.

## How to use

Install all the required modules:

    npm install

Before starting the server, you can change the listening port (default 10000) in the .env configuration file. To start the server:

    npm start

Now you can connect as many telnet client as you want by typing:

    telnet localhost <port-number>

Every one will receive a random id string, so the other partecipants can identify each other. The chat uses a different random color for each connected client, and each message i preceded by the senders id. The server message will always be in red and preceded by the "ADMIN" string. To stop the server simply press CTRL+c.
